Usage
===

Background image loading at once insteaded of chopped 

Install
---
`npm i alt-imageloader`

Props
---
- **background**  - (string) background image url
- **className** - (string) additional class for topmost parent
- **animationDelay** - (number) delay before image loads

Classes
---
- **alt-imageloader** - wrapper class
- **alt-imageloader__image** - image container
- **alt-imageloader__image--active** - image container after image loads

Import css
---
Css will be in `node_modules/alt-imageloader/src/AltImageloader.css`