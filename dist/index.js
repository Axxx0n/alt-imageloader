'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AltImageloader = function (_Component) {
    _inherits(AltImageloader, _Component);

    function AltImageloader() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, AltImageloader);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = AltImageloader.__proto__ || Object.getPrototypeOf(AltImageloader)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
            isLoaded: false
        }, _this.handleLoad = function () {
            setTimeout(function () {
                _this.setState({
                    isLoaded: true
                });
            }, _this.props.animationDelay);
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(AltImageloader, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            // load instantly when server side rendering
            if (this.img && this.img.complete) {
                this.handleLoad();
            }
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            if (this.props.background !== nextProps.background) {
                this.setState({
                    isLoaded: false
                });
            }
        }
    }, {
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps) {
            if (this.props.dontUpdate) {
                if (this.props.background === nextProps.background) {
                    return false;
                }
            }
            return true;
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var backgroundStyle = null;
            var activeClass = '';
            if (this.state.isLoaded) {
                backgroundStyle = {
                    backgroundImage: 'url(\'' + this.props.background + '\')'
                };
                activeClass = 'alt-imageloader__background--active';
            }

            if (this.props.isImage) {
                return _react2.default.createElement(
                    'div',
                    { className: 'alt-imageloader ' + this.props.className },
                    _react2.default.createElement('img', {
                        src: this.props.background,
                        className: 'alt-imageloader__image ' + activeClass,
                        onLoad: this.handleLoad.bind(this),
                        ref: function ref(img) {
                            _this2.img = img;
                        },
                        alt: 'imageLoader'
                    })
                );
            }
            return _react2.default.createElement(
                'div',
                {
                    className: 'alt-imageloader alt-imageloader--background ' + this.props.className
                },
                _react2.default.createElement('div', {
                    style: backgroundStyle,
                    className: 'alt-imageloader__background ' + activeClass
                }),
                _react2.default.createElement('img', {
                    src: this.props.background,
                    className: 'invisible',
                    onLoad: this.handleLoad.bind(this),
                    ref: function ref(img) {
                        _this2.img = img;
                    },
                    alt: 'placeholder'
                })
            );
        }
    }]);

    return AltImageloader;
}(_react.Component);

AltImageloader.defaultProps = {
    background: '',
    className: '',
    animationDelay: 200,
    isImage: false,
    handleOnClick: function handleOnClick() {},
    dontUpdate: false
};
exports.default = AltImageloader;