import React, { useEffect, useState, useRef } from 'react'

const AltImageloader = ({
    src = '',
    className = '',
    isImage = false
}) => {
    const [loadedSrc, setLoadedSrc] = useState('')
    const img = useRef(null)

    useEffect(() => {
        setLoadedSrc('')
    }, [src])

    useEffect(() => {
        if (img.current && img.current.complete) {
            handleLoad()
        }
    }, [])

    function handleLoad() {
        setLoadedSrc(src)
    }

    let backgroundStyle = null
    let activeClass = ''
    if (loadedSrc === src) {
        backgroundStyle = {
            backgroundImage: `url('${src}')`
        }
        activeClass = 'alt-imageloader__background--active'
    }

    if (isImage) {
        return (
            <div className={`alt-imageloader ${className}`}>
                <img
                    src={src}
                    className={`alt-imageloader__image ${activeClass}`}
                    onLoad={handleLoad}
                    ref={img}
                    alt="imageLoader"
                    loading="eager"
                />
            </div>
        )
    }
    return (
        <div className={`alt-imageloader alt-imageloader--background ${className}`}>
            <div
                style={backgroundStyle}
                className={`alt-imageloader__background ${activeClass}`}
            >
            </div>
            <img
                src={src}
                className="invisible"
                onLoad={handleLoad}
                ref={img}
                alt="placeholder"
                loading="eager"
            />
        </div>
    )
}

export default AltImageloader
